package sample.conditions;

import sample.util.Index2D;

import java.util.Iterator;
import java.util.List;

/**
 * Created by frben on 30.04.2016.
 */
public class NonPeriodicConditions implements BoundaryConditions {

    @Override
    public List<Index2D> checkConditions(List<Index2D> neighbours, Index2D size) {

        Iterator<Index2D> i = neighbours.iterator();
        while (i.hasNext()) {
            Index2D nbh = i.next();
            if(nbh.i == -1){
                i.remove();
            } else if(nbh.i == size.i){
                i.remove();
            } else if(nbh.j == -1){
                i.remove();
            } else if(nbh.j == size.j){
                i.remove();
            }
        }

        return neighbours;
    }

}
