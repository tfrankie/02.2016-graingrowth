package sample.conditions;

import sample.util.Index2D;

import java.util.Iterator;
import java.util.List;

/**
 * Created by frben on 30.04.2016.
 */
public class PeriodicConditions implements BoundaryConditions {

    @Override
    public List<Index2D> checkConditions(List<Index2D> neighbours, Index2D size) {

        Iterator<Index2D> i = neighbours.iterator();
        while (i.hasNext()) {
            Index2D nbh = i.next();

            if(nbh.i == -1){
                nbh.i = size.i-1;
            } else if(nbh.i == size.i){
                nbh.i = 0;
            }

            if(nbh.j == -1){
                nbh.j = size.j-1;
            } else if(nbh.j == size.j){
                nbh.j = 0;
            }
        }

        return neighbours;
    }
}
