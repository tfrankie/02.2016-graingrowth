package sample.conditions;

import sample.util.Index2D;

import java.util.List;

/**
 * Created by frben on 30.04.2016.
 */
public interface BoundaryConditions {

    List<Index2D> checkConditions(List<Index2D> neighbours, Index2D size);

}
