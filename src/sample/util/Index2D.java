package sample.util;

/**
 * Created by frben on 30.04.2016.
 */
public class Index2D {

    public int i,j;

    public Index2D(int i, int j) {
        this.i = i;
        this.j = j;
    }

    @Override
    public String toString() {
        return "i: " + i + " j: " + j;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Index2D index2D = (Index2D) o;

        if (i != index2D.i) return false;
        return j == index2D.j;

    }

    @Override
    public int hashCode() {
        int result = i;
        result = 31 * result + j;
        return result;
    }
}
