package sample.util;


import javafx.scene.paint.Color;

import java.util.Random;

/**
 * Created by frben on 30.04.2016.
 */
public class GrainState {

    private static Random random = new Random();

    private Color color;
    private double ro;
    private boolean empty;
    private boolean recrystalized;
    private boolean nbhRecrystalised;

    public GrainState() {
        empty = true;
        color = Color.WHITE;
        ro = 0;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getRo() {
        return ro;
    }

    public void setRo(double ro) {
        this.ro = ro;
    }

    public boolean isRecrystalized() {
        return recrystalized;
    }

    public void setRecrystalized(boolean recrystalized) {
        this.recrystalized = recrystalized;
    }

    public boolean isNbhRecrystalised() {
        return nbhRecrystalised;
    }

    public void setNbhRecrystalised(boolean nbhRecrystalised) {
        this.nbhRecrystalised = nbhRecrystalised;
    }

    public static Color getNextColor() {
        float r = random.nextFloat();
        float g = random.nextFloat();
        float b = random.nextFloat();

        return Color.color(r,g,b);
    }
}
