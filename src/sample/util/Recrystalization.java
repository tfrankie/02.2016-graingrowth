package sample.util;

/**
 * Created by frben on 28.05.2016.
 */
public class Recrystalization {

    private static double A = 86710969050178.5;
    private static double B = 9.41268203527779;

    public static double countRo(double t){
        return A/B + (1 - A/B)*Math.exp(-B*t);
    }

}
