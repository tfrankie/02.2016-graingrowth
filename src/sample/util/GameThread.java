package sample.util;

import sample.Controller;

/**
 * Created by frben on 30.04.2016.
 */
public class GameThread extends Thread {

    Controller ctrl;
    boolean stopGame;

    public GameThread(Controller ctrl) {
        this.ctrl = ctrl;
    }

    @Override
    public void run() {
        while(!stopGame){
            ctrl.onNextClick();
            try {
                sleep(ctrl.getGrowthDelayTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopGame() {
        stopGame = true;
    }

}
