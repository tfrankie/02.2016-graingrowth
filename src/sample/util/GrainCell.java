package sample.util;


import javafx.scene.paint.Color;

/**
 * Created by frben on 01.05.2016.
 */
public class GrainCell {

    Color color;
    Index2D index;

    public GrainCell(Index2D randomIndex, Color color) {
        this.color = color;
        this.index = randomIndex;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Index2D getIndex() {
        return index;
    }

    public void setIndex(Index2D index) {
        this.index = index;
    }
}
