package sample.util;

import sample.Controller;
import sample.random.NormalRandomGrains;

/**
 * Created by frben on 07.05.2016.
 */
public class RandomGrainsThread extends Thread {

    Controller ctrl;
    boolean stopRandomizing;

    public RandomGrainsThread(Controller ctrl) {
        this.ctrl = ctrl;
    }

    public void stopRandomizing() {
        stopRandomizing = true;
    }

    @Override
    public void run() {
        while(!stopRandomizing){
            ctrl.fillRandomCells(NormalRandomGrains.getRandomGrains(1, ctrl.getSize()));
            try {
                sleep(ctrl.getRandomDelayTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
