package sample.neighbours;

import sample.util.Index2D;

import java.util.List;
import java.util.Random;

/**
 * Created by student on 2016-05-05.
 */
public class HexRandomNbh extends Neighbourhood {

    @Override
    protected List<Index2D> getNeighbours(Index2D cellIndex) {
        if (new Random().nextBoolean()) {
            return new HexLeftNbh().getNeighbours(cellIndex);
        } else {
            return new HexRightNbh().getNeighbours(cellIndex);
        }
    }

}

