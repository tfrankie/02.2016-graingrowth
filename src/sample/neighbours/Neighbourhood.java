package sample.neighbours;

import sample.conditions.BoundaryConditions;
import sample.conditions.PeriodicConditions;
import sample.util.Index2D;

import java.util.List;

/**
 * Created by frben on 30.04.2016.
 */
public abstract class Neighbourhood {

    BoundaryConditions boundaryConditions = new PeriodicConditions();

    protected abstract List<Index2D> getNeighbours(Index2D cellIndex);

    public List<Index2D> getNeighbours(Index2D cellIndex, Index2D size){

        List<Index2D> neighbours = getNeighbours(cellIndex);
        neighbours = boundaryConditions.checkConditions(neighbours,size);

        return neighbours;
    }

    public void setBoundaryConditions(BoundaryConditions boundaryConditions) {
        this.boundaryConditions = boundaryConditions;
    }

}
