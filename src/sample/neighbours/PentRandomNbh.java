package sample.neighbours;

import sample.util.Index2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by student on 2016-05-05.
 */
public class PentRandomNbh extends Neighbourhood{

    @Override
    protected List<Index2D> getNeighbours(Index2D cellIndex) {

        if (new Random().nextBoolean()) {
            return new PentLeft().getNeighbours(cellIndex);
        } else {
            return new PentRight().getNeighbours(cellIndex);
        }
    }

    private class PentLeft extends Neighbourhood{
        @Override
        protected List<Index2D> getNeighbours(Index2D cellIndex) {
            List<Index2D> neighbours = new ArrayList<>();

            neighbours.add(new Index2D(cellIndex.i-1, cellIndex.j-1));
            neighbours.add(new Index2D(cellIndex.i-1, cellIndex.j));
            neighbours.add(new Index2D(cellIndex.i, cellIndex.j-1));
            neighbours.add(new Index2D(cellIndex.i+1, cellIndex.j-1));
            neighbours.add(new Index2D(cellIndex.i+1, cellIndex.j));

            return neighbours;
        }
    }

    private class PentRight extends Neighbourhood{
        @Override
        protected List<Index2D> getNeighbours(Index2D cellIndex) {
            List<Index2D> neighbours = new ArrayList<>();

            neighbours.add(new Index2D(cellIndex.i-1, cellIndex.j));
            neighbours.add(new Index2D(cellIndex.i-1, cellIndex.j+1));
            neighbours.add(new Index2D(cellIndex.i, cellIndex.j+1));
            neighbours.add(new Index2D(cellIndex.i+1, cellIndex.j));
            neighbours.add(new Index2D(cellIndex.i+1, cellIndex.j+1));

            return neighbours;
        }
    }
}