package sample.neighbours;

import sample.util.Index2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by frben on 30.04.2016.
 */
public class VonNeumannNbh extends Neighbourhood {

    @Override
    protected List<Index2D> getNeighbours(Index2D cellIndex) {
        List<Index2D> neighbours = new ArrayList<>();

        neighbours.add(new Index2D(cellIndex.i-1, cellIndex.j));
        neighbours.add(new Index2D(cellIndex.i+1, cellIndex.j));
        neighbours.add(new Index2D(cellIndex.i, cellIndex.j-1));
        neighbours.add(new Index2D(cellIndex.i, cellIndex.j+1));

        return neighbours;
    }
}
