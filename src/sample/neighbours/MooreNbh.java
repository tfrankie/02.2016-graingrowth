package sample.neighbours;

import sample.util.Index2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by frben on 02.05.2016.
 */
public class MooreNbh extends Neighbourhood {

    @Override
    public List<Index2D> getNeighbours(Index2D cellIndex) {
        List<Index2D> neighbours = new ArrayList<>();

        for(int i=cellIndex.i-1; i<=cellIndex.i+1; i++){
            for(int j=cellIndex.j-1; j<=cellIndex.j+1; j++){
                if( !(( i== cellIndex.i) && (j== cellIndex.j)) ){
                    neighbours.add(new Index2D(i, j));
                }
            }
        }
        return neighbours;
    }
}
