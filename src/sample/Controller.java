package sample;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import sample.conditions.BoundaryConditions;
import sample.conditions.NonPeriodicConditions;
import sample.conditions.PeriodicConditions;
import sample.neighbours.*;
import sample.random.EqualLengthRandomGrains;
import sample.random.NormalRandomGrains;
import sample.random.RadiusRandomGrains;
import sample.util.*;

import java.util.*;


public class Controller {

    static BoundaryConditions[] conditions = {new PeriodicConditions(), new NonPeriodicConditions()};
    static Neighbourhood[] neighbourhoods = {new VonNeumannNbh(), new MooreNbh(), new HexLeftNbh(), new HexRightNbh(), new HexRandomNbh(), new PentRandomNbh()};

    @FXML
    GridPane mainGrid;
    @FXML
    GridPane rectanglesPane;
    @FXML
    ChoiceBox conditionBox;
    @FXML
    ChoiceBox neighbourBox;
    @FXML
    ScrollBar delayGrowthTimeBar;
    private long growthDelayTime;
    @FXML
    ScrollBar delayRandomTimeBar;
    private long randomDelayTime;
    @FXML
    TextField radiusTf;
    @FXML
    TextField grainCountTf;
    @FXML
    TextField distanceTf;
    @FXML
    Button growthStartButton;
    @FXML
    Button randomStartButton;

    GrainState[][] tab;
    Rectangle[][] rectTab;
    Index2D size = new Index2D(260, 260); //260 //40
    int rectangleSize = 3; //3 //16

    Neighbourhood currentNeighbourhood;

    GameThread gameThread ;
    RandomGrainsThread randomizingThread;

    @FXML
    public void initialize(){
        initChoiseBoxes();
        initDelayTimeBars();

        tab = new GrainState[size.i][size.j];
        rectTab = new Rectangle[size.i][size.j];
        for(int i=0; i<size.i; i++){
            for(int j=0; j<size.j; j++){
                tab[i][j] = new GrainState();

                rectTab[i][j] = initRectangle(i,j);
                rectanglesPane.add(rectTab[i][j], j, i);
            }
        }
    }

    private void initDelayTimeBars() {
        growthDelayTime = 150;
        delayGrowthTimeBar.setValue(-150);
        delayGrowthTimeBar.valueProperty().addListener((ov, oldValue, newValue) -> {
            growthDelayTime = -newValue.longValue();
        });

        randomDelayTime = 150;
        delayRandomTimeBar.setValue(-150);
        delayRandomTimeBar.valueProperty().addListener((ov, oldValue, newValue) -> {
            randomDelayTime = -newValue.longValue();
        });
    }

    private void initChoiseBoxes() {
        currentNeighbourhood = neighbourhoods[0];
        currentNeighbourhood.setBoundaryConditions(conditions[0]);

        neighbourBox.setItems(FXCollections.observableArrayList("VonNeumann", "Moore",
                "Heksagonalne lewe", "Heksagonalne prawe", "Heksagonalne losowe", "Pentagonalne losowe"));
        neighbourBox.setValue("VonNeumann");
        neighbourBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            currentNeighbourhood = neighbourhoods[newValue.intValue()];
            currentNeighbourhood.setBoundaryConditions(conditions[conditionBox.getItems().indexOf(conditionBox.getValue())]);
        });

        conditionBox.setItems(FXCollections.observableArrayList("Periodyczne", "Nieperiodyczne"));
        conditionBox.setValue("Periodyczne");
        conditionBox.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            currentNeighbourhood.setBoundaryConditions(conditions[newValue.intValue()]);
        });
    }

    private Rectangle initRectangle(int i, int j) {
        Rectangle rectangle = new Rectangle(rectangleSize,rectangleSize);
        rectangle.setFill(Color.WHITE);
//        rectangle.setStroke(Color.BLACK);
        rectangle.setOnMouseClicked(event -> onRectangleClick(i,j));
        return rectangle;
    }

    private void onRectangleClick(int i, int j) {
        Color color = GrainState.getNextColor();
        changeGrainState(i, j, color);
    }

    private void changeGrainState(int i, int j, Color color) {
        if(color == Color.WHITE){
            tab[i][j].setEmpty(true);
            tab[i][j].setRo(0);
            tab[i][j].setRecrystalized(false);
        } else {
            tab[i][j].setEmpty(false);
        }
        tab[i][j].setColor(color);
        rectTab[i][j].setFill(tab[i][j].getColor());
    }

    public void onNextClick(){
        Map<Index2D, GrainState> stepMap = new HashMap<>();

        for(int i=0; i<size.i; i++) {
            for (int j = 0; j < size.j; j++) {
                if( !tab[i][j].isEmpty()){
                    List<Index2D> neighbours = currentNeighbourhood.getNeighbours(new Index2D(i,j), size);

                    for(Index2D nbhIndex: neighbours){
                        if(tab[nbhIndex.i][nbhIndex.j].isEmpty()){
                            stepMap.put(new Index2D(nbhIndex.i, nbhIndex.j), tab[i][j]);
                        }
                    }
                }
            }
        }

        for (Map.Entry<Index2D, GrainState> entry : stepMap.entrySet()) {
            Index2D key = entry.getKey();
            GrainState value = entry.getValue();
            changeGrainState(key.i, key.j, value.getColor());
        }

    }

    public void onClearClick(){
        time = 0;
        for(int i=0; i<size.i; i++){
            for(int j=0; j<size.j; j++) {
                changeGrainState(i, j, Color.WHITE);
            }
        }
    }

    public void onStartClick(){
        if(growthStartButton.getText().equals("Start")){
            gameThread = new GameThread(this);
            gameThread.start();
            growthStartButton.setText("Stop");
        } else {
            growthStartButton.setText("Start");
            gameThread.stopGame();
        }
    }

    public void onLosowoClick(){
        try{
            int cellCount = Integer.parseInt(grainCountTf.getText());

            onClearClick();
            fillRandomCells(NormalRandomGrains.getRandomGrains(cellCount, size));
        } catch(NumberFormatException e){
            AlertBox.display("Uzupełnij ilosc ziaren");
        }
    }

    public void onRownomiernieClick(){
        try{
            int distance = Integer.parseInt(distanceTf.getText());

            onClearClick();
            fillRandomCells(EqualLengthRandomGrains.getRandomGrains(distance, size));
        } catch(NumberFormatException e){
            AlertBox.display("Uzupełnij odległość");
        }
    }

    public void onLosoweRClick(){
        int cellCount;
        int radius;
        try{
            cellCount = Integer.parseInt(grainCountTf.getText());
            radius = Integer.parseInt(radiusTf.getText());

            onClearClick();
            fillRandomCells(RadiusRandomGrains.getRandomGrains(cellCount, radius, size));
        } catch(NumberFormatException e){
            AlertBox.display("Uzupełnij ilosc ziaren oraz R");
        } catch (Exception e) {
            AlertBox.display("Podano złe parametry");
        }
    }

    public void fillRandomCells(List<GrainCell> grains) {
        for(GrainCell cell: grains){
            Index2D index = cell.getIndex();
            if(tab[index.i][index.j].isEmpty()){
                changeGrainState(index.i, index.j, cell.getColor());
            }
        }
    }

    List<Index2D> monteCarloList = new ArrayList<>();

    public void onCiagleLosowanieClick() {
        if(randomStartButton.getText().equals("Start")){
            randomizingThread = new RandomGrainsThread(this);
            randomizingThread.start();
            randomStartButton.setText("Stop");
        } else {
            randomStartButton.setText("Start");
            randomizingThread.stopRandomizing();
        }
    }

    public void onMonteCarloStart(){
        for(int i=0; i<size.i; i++){
            for(int j=0; j<size.j; j++) {
                monteCarloList.add(new Index2D(i,j));
                changeGrainState(i, j, GrainState.getNextColor());
            }
        }
    }

    public void onMonteCarloStep(){
        while(monteCarloList.size()>0){
            Index2D currentIndex = monteCarloList.get(random.nextInt(monteCarloList.size()));

            int energy = countGrainEnergy(currentIndex);

            monteCarloList.remove(currentIndex);
        }
    }

    private int countGrainEnergy(Index2D index) {
        Color ownerColor = tab[index.i][index.j].getColor();
        int sum = 0;
        List<Index2D> neighbours = neighbourhoods[1].getNeighbours(index, size);
        for(Index2D nbh: neighbours){
            if(ownerColor != tab[nbh.i][nbh.j].getColor()){
                sum++;
            }
        }
        return sum;
    }

    private double time = 0;
    private final double step = 0.001;
    private final double criticalRo = 4215840142323.42 / (size.j * size.j);
    private Random random = new Random();

    public void onRecrystalizationClick(){
        try{
            oneRecrystalizationStep();
        } catch (IllegalArgumentException exception){
            AlertBox.display("Uzupełnij plansze");
        }
    }

    public void oneRecrystalizationStep(){
        List<Index2D> borderGrains = new ArrayList<>();

        time += step;
        double ro = Recrystalization.countRo(time) - Recrystalization.countRo(time-step);
        double singleCellRo = ro / (size.i * size.j);
        double restOfRo = ro;

        checkIfNeighboursRecrystalized();

        for(int i=0; i<size.i; i++){
            for(int j=0; j<size.j; j++) {
                if( isOnTheBorder(i,j) ){
                    tab[i][j].setRo(tab[i][j].getRo() + (0.8 * singleCellRo));
                    restOfRo -= 0.8 * singleCellRo;
                    borderGrains.add(new Index2D(i,j));
                } else {
                    tab[i][j].setRo(tab[i][j].getRo() + (0.2 * singleCellRo));
                    restOfRo -= 0.2 * singleCellRo;
                }
            }
        }

        int randomGrainsCount = 5;
        singleCellRo = restOfRo/randomGrainsCount;
        for(int i=0; i<randomGrainsCount; i++){
            Index2D randomBorderGrain = borderGrains.get(random.nextInt(borderGrains.size()));
            tab[randomBorderGrain.i][randomBorderGrain.j].setRo(tab[randomBorderGrain.i][randomBorderGrain.j].getRo() + singleCellRo);
        }

        for(int i=0; i<size.i; i++){
            for(int j=0; j<size.j; j++) {
                if( (!tab[i][j].isRecrystalized()) && tab[i][j].getRo() > criticalRo){
                    tab[i][j].setRecrystalized(true);
                    changeGrainState(i,j,GrainState.getNextColor());
                }
            }
        }
    }

    private void checkIfNeighboursRecrystalized() {
        Map<Index2D, GrainState> recrtystalizationMap = new HashMap<>();

        for(int i=0; i<size.i; i++) {
            for (int j = 0; j < size.j; j++) {

                if(!tab[i][j].isRecrystalized()){
                    List<Index2D> neighbours = currentNeighbourhood.getNeighbours(new Index2D(i,j), size);

                    for(Index2D neighbour: neighbours){
                        if( tab[neighbour.i][neighbour.j].isRecrystalized() ){
                            recrtystalizationMap.put(new Index2D(i, j), tab[neighbour.i][neighbour.j]);
                        }
                    }
                }
            }
        }

        for (Map.Entry<Index2D, GrainState> entry : recrtystalizationMap.entrySet()) {
            Index2D key = entry.getKey();
            GrainState value = entry.getValue();
            tab[key.i][key.j].setRecrystalized(true);
            changeGrainState(key.i, key.j, value.getColor());
        }
    }


    private boolean isOnTheBorder(int i, int j) {
        Color ownerColor = tab[i][j].getColor();
        List<Index2D> neighbours = neighbourhoods[1].getNeighbours(new Index2D(i,j), size);

        for(Index2D neighbour: neighbours){
            if( !(tab[neighbour.i][neighbour.j].getColor().equals(ownerColor)) ){
                return true;
            }
        }
        return false;
    }

    public Index2D getSize() {
        return size;
    }
    public long getGrowthDelayTime() {
        return growthDelayTime;
    }
    public long getRandomDelayTime(){
        return randomDelayTime;
    }

}
