package sample.random;

import javafx.scene.paint.Color;
import sample.util.GrainCell;
import sample.util.GrainState;
import sample.util.Index2D;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by frben on 02.05.2016.
 */
public class RadiusRandomGrains {

    static Random random = new Random();
    static int methodMaxDuration = 1500;

    public static List<GrainCell> getRandomGrains(int count, int radius, Index2D size) throws Exception {
        Instant beggining = Instant.now();
        List<GrainCell> grainsList = new ArrayList<>();

        while ( grainsList.size() < count ) {

            GrainCell grainCell = getRandomCell(size);

            boolean rightDistance = true;
            for (GrainCell cell : grainsList) {
                if ((countDistance(cell.getIndex(), grainCell.getIndex())) < radius) {
                    rightDistance = false;
                }
            }
            if(rightDistance){
                grainsList.add(grainCell);
            }


            if(Duration.between(beggining, Instant.now()).toMillis() > methodMaxDuration){
                throw new Exception();
            }
        }

        return grainsList;
    }

    public static int countDistance(Index2D first, Index2D second){
        return (int) Math.sqrt(Math.pow(second.i - first.i,2) + Math.pow(second.j - first.j,2));
    }

    public static GrainCell getRandomCell(Index2D size) {
        Index2D randomIndex = new Index2D(random.nextInt(size.i), random.nextInt(size.j));
        Color color = GrainState.getNextColor();
        return new GrainCell(randomIndex, color);
    }
}
