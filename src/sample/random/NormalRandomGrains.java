package sample.random;

import javafx.scene.paint.Color;
import sample.util.GrainCell;
import sample.util.GrainState;
import sample.util.Index2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by frben on 01.05.2016.
 */
public class NormalRandomGrains{

    public static List<GrainCell> getRandomGrains(int count, Index2D size) {

        List<GrainCell> grainsList = new ArrayList<>();

        Random random = new Random();
        for(int i=0; i<count; i++){
            Index2D randomIndex = new Index2D(random.nextInt(size.i), random.nextInt(size.j));
            Color color = GrainState.getNextColor();
            GrainCell grain = new GrainCell(randomIndex, color);
            grainsList.add(grain);
        }

        return grainsList;
    }
}
