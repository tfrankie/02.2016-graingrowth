package sample.random;

import javafx.scene.paint.Color;
import sample.util.GrainCell;
import sample.util.GrainState;
import sample.util.Index2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by frben on 01.05.2016.
 */
public class EqualLengthRandomGrains {

    public static List<GrainCell> getRandomGrains(int distance, Index2D size) {

        List<GrainCell> grainsList = new ArrayList<>();
        int currentI = distance/2;
        int currentJ = currentI;

        while(currentI < size.i){
            Index2D randomIndex = new Index2D(currentI, currentJ);
            Color color = GrainState.getNextColor();
            GrainCell grain = new GrainCell(randomIndex, color);
            grainsList.add(grain);

            if(currentJ < (size.j - distance)){
                currentJ += distance;
            } else {
                currentJ = distance/2;
                currentI += distance;
            }
        }

        return grainsList;
    }

}
